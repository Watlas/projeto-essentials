package com.watlas.projetoessentials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoEssentialsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetoEssentialsApplication.class, args);
    }

}
